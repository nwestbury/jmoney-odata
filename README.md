# JMoney OData

An OData implementation that provides accounting data from a JMoney database stored in Derby.

This is almost certainly only of use to you if you are looking for an example of how to implement an OData provider against Derby or another SQL database.  The schema is fixed.  It is implemented in Java using Olingo.

This OData provider is being used with Microsoft BI.  It has not been tested with other consumers.

=== Starting the Server ===

After checkout out the repository:

`mvn tomcat:run`

Now that is only going to work if you have a Derby database with exactly the correct schema
at exactly the hard-coded location.  Also the database cannot be already open because the Derby
driver is embedded in the OData server.  You are not likely to have such a database so you will
need to hack around to get this to work.

Then from a browser go to http://localhost:8080/odata-server-sample/cars.svc

or to see the accounts, http://localhost:8080/odata-server-sample/cars.svc/Accounts

Entries are also supported.

and you can fetch a single entity given the id:

http://localhost:8080/odata-server-sample/cars.svc/Entries(3)

The following are supported:

$top, $skip, $count