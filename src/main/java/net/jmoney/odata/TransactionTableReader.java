/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.Constants;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Link;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class TransactionTableReader extends TableReader {

	private TableReader entryTable;
	
	private PreparedStatement lookupById;

	private PreparedStatement entriesByTransaction;
	
	private PreparedStatement entriesByTransactionAndEntryId;

	public TransactionTableReader(Connection connection) throws SQLException {
		super(connection);

		properties.put("Date", new AccountingProperty("\"date\"", Type.DATE));
	}

	@Override
	public void init(TableReaderFactory readerFactory) throws SQLException {
		entryTable = readerFactory.getEntryTableReader();

		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where \"_ID\" = ?");
		entriesByTransaction = connection.prepareStatement("select " + entryTable.getColumnSelectors() + " from " + entryTable.getTableJoins() + " where \"net_sf_jmoney_transaction_entry\" = ?");
		entriesByTransactionAndEntryId = connection.prepareStatement("select " + entryTable.getColumnSelectors() + " from " + entryTable.getTableJoins() + " where \"net_sf_jmoney_transaction_entry\" = ? and \"_ID\" = ?");

		navigationCollectionProperties.put("Entries",
				new ExpandedCollection("Entries", entryTable, "\"net_sf_jmoney_transaction_entry\""));
	}
	
	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_TRANSACTION";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_TRANSACTION";
	}
	
	@Override
	public String getColumnSelectors() {
		return "NET_SF_JMONEY_TRANSACTION.\"_ID\" as TRANSACTION_ID, \"date\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "TRANSACTION_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		int id = result.getInt("TRANSACTION_ID");
		Date transactionDate = result.getDate("date");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Date", transactionDate));
		entity.setId(createId(AccountsEdmProvider.ES_TRANSACTIONS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_TRANSACTION.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("TRANSACTION_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_TRANSACTIONS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_TRANSACTION.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			
			// Now process expanded collections.  These are done as a separate query
			// because a join would result in multiple rows for the above read.
			List<ExpandedCollection> expandedCollections = buildExpandedCollections(expandOption);
			for (ExpandedCollection expandedCollection: expandedCollections) {
				String basicQuery = "select " + expandedCollection.getReader().getColumnSelectors() + " from " + expandedCollection.getReader().getTableJoins()
						+ " where " + expandedCollection.foreignKeyName + " = ?"; 
				
					PreparedStatement stmt = this.connection.prepareStatement(basicQuery);
					stmt.setInt(1, key);
					ResultSet result = stmt.executeQuery();
					TableReader readerForExpandedItem = expandedCollection.getReader();
					
					EntityCollection inlineEntities = new EntityCollection();
					while (result.next()) {
						// Currently nested expanded items are not supported so we pass on the empty list
						Entity inlineEntity = readerForExpandedItem.createEntityFromResult(result, Collections.<Expansion>emptyList());
						inlineEntities.getEntities().add(inlineEntity);
					}
					
					Link link = new Link();
					link.setTitle(expandedCollection.getNavigationPropertyName());
			        link.setType(Constants.ENTITY_NAVIGATION_LINK_TYPE);
			        link.setRel(Constants.NS_ASSOCIATION_LINK_REL + expandedCollection.getNavigationPropertyName());
					link.setInlineEntitySet(inlineEntities);
		            link.setHref(thisOne.getId().toASCIIString() + '/' + expandedCollection.getNavigationPropertyName());
					thisOne.getNavigationLinks().add(link);
			}
			
			return thisOne;
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			// There must be a key in this case in the URL after Entries
			List<UriParameter> keys = navProperty.getKeyPredicates();
			if (keys.size() != 1) {
				throw new RuntimeException("table must have one key field: " + this.toString());
			}
			String keyValueText = keys.get(0).getText();
			int entryKeyValue = Integer.valueOf(keyValueText);
			
			entriesByTransactionAndEntryId.setInt(1, key);
			entriesByTransactionAndEntryId.setInt(2, entryKeyValue);
			ResultSet result = entriesByTransactionAndEntryId.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result as an entity
				result.next();
				List<Expansion> expandedItems = buildExpandedItems(expandOption);
				Entity el = entryTable.createEntityFromResult(result, expandedItems);
				return el;
			} else {
				// There are more parts to join
				result.next();
				int entryId = result.getInt("_ID");
				Entity entityForThisEntry = entryTable.readAndJoin(uriInfo, resourceParts, index + 1, entryId, expandOption); 
				return entityForThisEntry;
			}
		}
		return null; // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}

		EntityCollection entitySet = new EntityCollection();

		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			PreparedStatement stmt = null;
			try {
				if (index + 2 == resourceParts.size()) {
					// This is the last part, so the filter applies here
					ResultSet result;
					if (filterOption != null) {
						String whereClause = buildWhereClause(filterOption);
						stmt = connection.prepareStatement("select " + entryTable.getColumnSelectors() + " from NET_SF_JMONEY_ENTRY where \"net_sf_jmoney_transaction_entry\" = ? and " + whereClause);
						stmt.setInt(1, key);
						result = stmt.executeQuery();
					} else {
						entriesByTransaction.setInt(1, key);
						result = entriesByTransaction.executeQuery();
					}

					// This is the last part, so return this result set as entities

					while (result.next()) {
						Entity el = entryTable.createEntityFromResult(result, Collections.<Expansion>emptyList());
						entitySet.getEntities().add(el);
					}

					return entitySet;
				} else {
					// There are more parts so filter is not applied at this time
					entriesByTransaction.setInt(1, key);
					ResultSet result = entriesByTransaction.executeQuery();

					// There are more parts to join
					while (result.next()) {
						int entryId = result.getInt("_ID");
						EntityCollection entitiesForThisEntry = entryTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, entryId, filterOption, expandOption); 
						entitySet.getEntities().addAll(entitiesForThisEntry.getEntities());
					}

					return entitySet;
				}
			} finally {
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		return null; // TODO
	}
}
