package net.jmoney.odata;

import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.edm.EdmEnumType;
import org.apache.olingo.commons.api.edm.EdmType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.commons.core.edm.primitivetype.EdmString;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourcePrimitiveProperty;
import org.apache.olingo.server.api.uri.queryoption.expression.BinaryOperatorKind;
import org.apache.olingo.server.api.uri.queryoption.expression.Expression;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitException;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitor;
import org.apache.olingo.server.api.uri.queryoption.expression.Literal;
import org.apache.olingo.server.api.uri.queryoption.expression.Member;
import org.apache.olingo.server.api.uri.queryoption.expression.MethodKind;
import org.apache.olingo.server.api.uri.queryoption.expression.UnaryOperatorKind;

import net.jmoney.odata.WherePart.Type;

public class FilterExpressionToWhereClause implements ExpressionVisitor<WherePart> {

	/**
	 * The table from which properties in the query are resolved from.
	 */
	private TableReader tableReader;

	public FilterExpressionToWhereClause(TableReader tableReader) {
	    this.tableReader = tableReader;
	}
	
	@Override
	public WherePart visitBinaryOperator(BinaryOperatorKind operator, WherePart left, WherePart right)
			throws ExpressionVisitException, ODataApplicationException {

	    // Binary Operators are split up in three different kinds. Up to the kind of the
	    // operator it can be applied to different types
	    //   - Arithmetic operations like add, minus, modulo, etc. are allowed on numeric
	    //     types like Edm.Int32
	    //   - Logical operations are allowed on numeric types and also Edm.String
	    //   - Boolean operations like and, or are allowed on Edm.Boolean
	    // A detailed explanation can be found in OData Version 4.0 Part 2: URL Conventions

		if (right.isNull()) {
			switch (operator) {
			case EQ:
				return new WherePart(left.getSql() + " IS NULL", Type.BOOLEAN);
			case NE:
				return new WherePart(left.getSql() + " IS NOT NULL", Type.BOOLEAN);
			default:
				throw new ODataApplicationException(
						"Binary operation " + operator.name() + " accepts only boolean operands",
						HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
						Locale.ENGLISH);
			}
		}
		
		switch (operator) {
		case AND:
		case OR:
			if (!left.isBoolean() || !right.isBoolean()) {
		        throw new ODataApplicationException(
		        		"Binary operation " + operator.name() + " accepts only boolean operands",
		        		HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
		        		Locale.ENGLISH);
			}
			break;
	    default:
			if (!left.isEqualInType(right)) {
		        throw new ODataApplicationException(
		        		"Binary operation " + operator.name() + " accepts only operands of matching types",
		        		HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
		        		Locale.ENGLISH);
			}
			break;
		}
		
		switch (operator) {
		case ADD:
			return new WherePart("(" + left.getSql() + " + " + right.getSql() + ")", Type.INTEGER);
		case MOD:
			return new WherePart("(" + left.getSql() + " MOD " + right.getSql() + ")", Type.INTEGER);
		case MUL:
			return new WherePart("(" + left.getSql() + " * " + right.getSql() + ")", Type.INTEGER);
		case DIV:
			return new WherePart("(" + left.getSql() + " / " + right.getSql() + ")", Type.INTEGER);
		case SUB:
			return new WherePart("(" + left.getSql() + " - " + right.getSql() + ")", Type.INTEGER);
			
		case EQ:
			return new WherePart(left.getSql() + " = " + right.getSql(), Type.BOOLEAN);
		case NE:
			return new WherePart(left.getSql() + " <> " + right.getSql(), Type.BOOLEAN);
		case GE:
			return new WherePart(left.getSql() + " >= " + right.getSql(), Type.BOOLEAN);
		case GT:
			return new WherePart(left.getSql() + " > " + right.getSql(), Type.BOOLEAN);
		case LE:
			return new WherePart(left.getSql() + " <= " + right.getSql(), Type.BOOLEAN);
		case LT:
			return new WherePart(left.getSql() + " < " + right.getSql(), Type.BOOLEAN);
			
		case AND:
			return new WherePart("(" + left.getSql() + " AND " + right.getSql() + ")", Type.BOOLEAN);
		case OR:
			return new WherePart("(" + left.getSql() + " OR " + right.getSql() + ")", Type.BOOLEAN);
			
	    default:
	        throw new ODataApplicationException(
	        		"Binary operation " + operator.name() + " is not implemented",
	        		HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
	        		Locale.ENGLISH);
		}
	}

	@Override
	public WherePart visitUnaryOperator(UnaryOperatorKind operator, WherePart operand)
			throws ExpressionVisitException, ODataApplicationException {
		  // OData allows two different unary operators. We have to take care, that the type of the
	    // operand fits to the operand

	    if (operator == UnaryOperatorKind.NOT && operand.isBoolean()) {
	      // 1.) boolean negation
			return new WherePart("NOT(" + operand.getSql() + ")", Type.BOOLEAN);
	    } else if (operator == UnaryOperatorKind.MINUS && operand.isInteger()) {
	      // 2.) arithmetic minus
			return new WherePart("-(" + operand.getSql() + ")", Type.INTEGER);
	    }

	    // Operation not processed, throw an exception
	    throw new ODataApplicationException("Invalid type for unary operator",
	        HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);	}

	@Override
	public WherePart visitMethodCall(MethodKind methodCall, List<WherePart> parameters)
			throws ExpressionVisitException, ODataApplicationException {
		// To keep this tutorial small and simple, we implement only one method call
	    // contains(String, String) -> Boolean
	    switch (methodCall) {
	    case CONTAINS:
	    	if (parameters.get(0).isString() && parameters.get(1).isString()) {
	    		// This concatenation operator is Derby only
	    		return new WherePart("(" + parameters.get(0).getSql() + " like ('%' || " + parameters.get(1).getSql() + " || '%'))", Type.BOOLEAN);
	    	} else {
	    		throw new ODataApplicationException("Contains needs two parametes of type Edm.String",
	    				HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
	    	}
	    default:
	    	throw new ODataApplicationException("Method call " + methodCall + " not implemented",
	    			HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
	    }
	}

	@Override
	public WherePart visitLambdaExpression(String lambdaFunction, String lambdaVariable, Expression expression)
			throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WherePart visitLiteral(Literal literal) throws ExpressionVisitException, ODataApplicationException {
	    // To keep this tutorial simple, our filter expression visitor supports only Edm.Int32 and Edm.String
	    // In real world scenarios it can be difficult to guess the type of an literal.
	    // We can be sure, that the literal is a valid OData literal because the URI Parser checks
	    // the lexicographical structure

	    // String literals start and end with an single quotation mark
	    String literalAsString = literal.getText();
	    if (literal.getType() == null) {
	    	return new WherePart(null, Type.NULL);
	    } else if (literal.getType() instanceof EdmString) {
	        String stringLiteral = "";
	        if(literal.getText().length() > 2) {
	            stringLiteral = literalAsString.substring(1, literalAsString.length() - 1);
	        }

	        // TODO entitize this, or pass as parameter.
            return new WherePart("'" + stringLiteral + "'", Type.STRING);
	    } else {
	        // Try to convert the literal into an Java Integer
	        try {
	            int intValue = Integer.parseInt(literalAsString);
	            return new WherePart(literalAsString, Type.INTEGER);
	        } catch(NumberFormatException e) {
	            throw new ODataApplicationException("Only Edm.Int32 and Edm.String literals are implemented",
	                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
	        }
	    }
	}

	@Override
	public WherePart visitMember(Member member) throws ExpressionVisitException, ODataApplicationException {
		 // To keeps things simple, this tutorial allows only primitive properties.
	    // We have faith that the java type of Edm.Int32 is Integer
	    final List<UriResource> uriResourceParts = member.getResourcePath().getUriResourceParts();

	    // Make sure that the resource path of the property contains only a single segment and a
	    // primitive property has been addressed. We can be sure, that the property exists because  
	    // the UriParser checks if the property has been defined in service metadata document.

	    if (uriResourceParts.size() == 1 && uriResourceParts.get(0) instanceof UriResourcePrimitiveProperty) {
	      UriResourcePrimitiveProperty uriResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts.get(0);
	      return tableReader.getSqlForProperty(uriResourceProperty.getProperty());
	    } else {
	      // The OData specification allows in addition complex properties and navigation    
	      // properties with a target cardinality 0..1 or 1.
	      // This means any combination can occur e.g. Supplier/Address/City
	      //  -> Navigation properties  Supplier
	      //  -> Complex Property       Address
	      //  -> Primitive Property     City
	      // For such cases the resource path returns a list of UriResourceParts
	      throw new ODataApplicationException(
	    		  "Only primitive properties are implemented in filter expressions",
	    		  HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
	    		  Locale.ENGLISH);
	    }
	}

	@Override
	public WherePart visitAlias(String aliasName) throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WherePart visitTypeLiteral(EdmType type) throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WherePart visitLambdaReference(String variableName)
			throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WherePart visitEnum(EdmEnumType type, List<String> enumValues)
			throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}
}
