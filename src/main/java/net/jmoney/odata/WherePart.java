package net.jmoney.odata;

public class WherePart {

	public enum Type {
		INTEGER,
		BOOLEAN,
		STRING,
		DATE,
		NULL
	}

	private String sqlFragment;
	private Type type;
	
	public WherePart(String sqlFragment, Type type) {
		this.sqlFragment = sqlFragment;
		this.type = type;
	}

	public String getSql() {
		return sqlFragment;
	}

	public boolean isBoolean() {
		return type == Type.BOOLEAN;
	}

	public boolean isInteger() {
		return type == Type.INTEGER;
	}

	public boolean isString() {
		return type == Type.STRING;
	}

	public boolean isNull() {
		return type == Type.NULL;
	}

	public boolean isEqualInType(WherePart other) {
		return type != Type.NULL && type == other.type;
	}

}
