/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class IncomeExpenseAccountTable extends TableReader {

	private AccountTableReader accountTable;
	
	private PreparedStatement lookupById;

	private PreparedStatement subAccountsByAccount;

	private PreparedStatement parentOfSubAccount;

	public IncomeExpenseAccountTable(Connection connection) throws SQLException {
		super(connection);

		properties.put("Name", new AccountingProperty("\"name\"", Type.STRING));
		properties.put("TaxCategory", new AccountingProperty("\"net_sf_jmoney_excel_taxProperties_taxCategory\"", Type.STRING));

		navigationProperties.put("ParentAccount",
				new ExpandedItem("ParentAccount", this, "\"net_sf_jmoney_categoryAccount_subAccount\""));
	}

	@Override
	public void init(TableReaderFactory tableFactory) throws SQLException {
		accountTable = tableFactory.getAccountTableReader();

		parentOfSubAccount = connection.prepareStatement("select " + getColumnSelectors() + " from NET_SF_JMONEY_CATEGORYACCOUNT as CHILD join NET_SF_JMONEY_CATEGORYACCOUNT on CHILD.\"net_sf_jmoney_categoryAccount_subAccount\" = NET_SF_JMONEY_CATEGORYACCOUNT.\"_ID\" join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_ACCOUNT.\"_ID\" = NET_SF_JMONEY_CATEGORYACCOUNT.\"_ID\" where CHILD.\"_ID\" = ?");

		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where NET_SF_JMONEY_ACCOUNT.\"_ID\" = ?");
		subAccountsByAccount = connection.prepareStatement("select " + getColumnSelectors() + " from " + getColumnSelectors() + " where \"net_sf_jmoney_categoryAccount_subAccount\" = ?");
	}

	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_CATEGORYACCOUNT inner join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_CATEGORYACCOUNT.\"_ID\" = NET_SF_JMONEY_ACCOUNT.\"_ID\"";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_CATEGORYACCOUNT";
	}
	
	@Override
	public String getColumnSelectors() {
		// The accounting tables may self-join (to get parent), so qualify all columns with the table id.
		return accountTable.getColumnSelectors() + ", NET_SF_JMONEY_CATEGORYACCOUNT.\"net_sf_jmoney_excel_taxProperties_taxCategory\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "ACCOUNT_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		/* 
		 * We don't return the decimal places here because all amounts are converted from long
		 * to decimal by this driver.  The number of decimal places is really used for conversion
		 * of money amounts to pennies for internal storage of integer values.  Possibly consumers
		 * could use this information for formatting or rounding purposes but that is unlikely.
		 */
		
		int id = result.getInt("ACCOUNT_ID");
		String name = result.getString("ACCOUNT_NAME");
		String taxCategory = result.getString("net_sf_jmoney_excel_taxProperties_taxCategory");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Name", name))
				.addProperty(createPrimitive("TaxCategory", taxCategory));
		entity.setId(createId(AccountsEdmProvider.ES_INCOME_EXPENSE_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_INCOME_EXPENSE_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("ACCOUNT_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_INCOME_EXPENSE_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_INCOME_EXPENSE_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		IncomeExpenseAccountTable incomeExpenseAccountTable = this;
		
		switch(navProp.getName()) {
		case "ParentAccount":
			parentOfSubAccount.setInt(1, key);
			ResultSet parentAccountResult = parentOfSubAccount.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
				parentAccountResult.next();
				List<Expansion> expandedItems = buildExpandedItems(expandOption);
				Entity el = incomeExpenseAccountTable.createEntityFromResult(parentAccountResult, expandedItems);
				return el;
			} else {
				// There are more parts to join
				parentAccountResult.next();
				int parentAccountId = parentAccountResult.getInt("ACCOUNT_ID");
				Entity entityForThisAccount = incomeExpenseAccountTable.readAndJoin(uriInfo, resourceParts, index + 1, parentAccountId, expandOption); 
				return entityForThisAccount;
			}

		}
		return null; // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			return accountTable.readEntries(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "ParentAccount":
			return readParentAccount(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "SubAccounts":
			return readSubAccounts(uriInfo, resourceParts, index, key, filterOption, expandOption);
		}
		
		throw new ODataApplicationException("Unexpected navigation from Category: " + navProp.getName(), 1, Locale.ENGLISH);
	}

	public EntityCollection readSubAccounts(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			FilterOption filterOption, ExpandOption expandOption)
			throws ODataApplicationException, SQLException {

		IncomeExpenseAccountTable incomeExpenseAccountTable = this;
		
		EntityCollection entitySet = new EntityCollection();

		if (index + 2 == resourceParts.size()) {
			// This is the last part, so return this result set as entities

			
			// TODO much of this is duplicated in AccountTable.executeQuery.
			// We should call a method on entryTable to do this.
			
			// Because this is the last part, we check now for expanded items.
			List<Expansion> expandedItems = incomeExpenseAccountTable.buildExpandedItems(expandOption);
			
			String columnsPartOfQuery = incomeExpenseAccountTable.getColumnSelectors();
			String tablesPartOfQuery = "NET_SF_JMONEY_CATEGORYACCOUNT left outer join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_CATEGORYACCOUNT.\"_ID\" = NET_SF_JMONEY_ACCOUNT.\"_ID\"";

			for (Expansion expansion : expandedItems) {
				ExpandedItem expandedItem = expansion.navigationProperty; 
				columnsPartOfQuery += ", " + expandedItem.getReader().getColumnSelectors();
				tablesPartOfQuery += " left outer join " + expandedItem.getReader().getTableName() + " on NET_SF_JMONEY_CATEGORYACCOUNT." + expandedItem.foreignKeyName + " = " + expandedItem.getReader().getTableName() + ".\"_ID\"";
			}
			
			String basicQuery = "select " + columnsPartOfQuery + " from " + tablesPartOfQuery + " where \"net_sf_jmoney_categoryAccount_subAccount\" = ?";
			if (filterOption != null) {
				String whereClause = buildWhereClause(filterOption);
				basicQuery = basicQuery + " where " + whereClause;
			}
			
			PreparedStatement entriesByAccount = connection.prepareStatement(basicQuery);
			
			entriesByAccount.setInt(1, key);
			ResultSet result = entriesByAccount.executeQuery();

			while (result.next()) {
				Entity el = incomeExpenseAccountTable.createEntityFromResult(result, expandedItems);
				entitySet.getEntities().add(el);
			}

			return entitySet;
		} else {
			// There are more parts to join
			subAccountsByAccount.setInt(1, key);
			ResultSet result = subAccountsByAccount.executeQuery();

			while (result.next()) {
				int accountId = result.getInt("ACCOUNT_ID");
				EntityCollection entitiesForThisSubAccount = this.readAndJoinCollection(uriInfo, resourceParts, index + 1, accountId, filterOption, expandOption); 
				entitySet.getEntities().addAll(entitiesForThisSubAccount.getEntities());
			}

			return entitySet;
		}
	}

	public EntityCollection readParentAccount(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			FilterOption filterOption, ExpandOption expandOption)
			throws SQLException, ODataApplicationException {

		IncomeExpenseAccountTable incomeExpenseAccountTable = this;
		
		parentOfSubAccount.setInt(1, key);
		ResultSet parentAccountResult = parentOfSubAccount.executeQuery();

		if (index + 2 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		} else {
			// There are more parts to join
			parentAccountResult.next();
			int parentAccountId = parentAccountResult.getInt("ACCOUNT_ID");
			EntityCollection entitiesForThisParentAccount = incomeExpenseAccountTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, parentAccountId, filterOption, expandOption); 
			return entitiesForThisParentAccount;
		}
	}
}
