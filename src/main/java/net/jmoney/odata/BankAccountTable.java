/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class BankAccountTable extends TableReader {

	private AccountTableReader accountTable;
	private CapitalAccountTable capitalAccountTable;
	
	private PreparedStatement lookupById;

	public BankAccountTable(Connection connection) throws SQLException {
		super(connection);

		properties.put("Name", new AccountingProperty("\"name\"", Type.STRING));

		properties.put("Comment", new AccountingProperty("\"comment\"", Type.STRING));
		properties.put("Abbreviation", new AccountingProperty("\"abbreviation\"", Type.STRING));

		properties.put("JMFinnName", new AccountingProperty("\"net_sf_jmoney_jmfinn_jmFinnAccount_finnAccountName\"", Type.STRING));
		properties.put("Comment", new AccountingProperty("\"minBalance\"", Type.INTEGER));
		properties.put("AccountNumber", new AccountingProperty("\"accountNumber\"", Type.STRING));
		properties.put("Bank", new AccountingProperty("\"accountNumber\"", Type.STRING));

		navigationProperties.put("ParentAccount",
				new ExpandedItem("ParentAccount", this, "\"net_sf_jmoney_capitalAccount_subAccount\""));
	}

	@Override
	public void init(TableReaderFactory tableFactory) throws SQLException {
		accountTable = tableFactory.getAccountTableReader();
		capitalAccountTable = tableFactory.getCapitalAccountTable();

		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where NET_SF_JMONEY_ACCOUNT.\"_ID\" = ?");
	}

	@Override
	public String getTableJoins() {
		return capitalAccountTable.getTableJoins() + " inner join NET_SF_JMONEY_BANKACCOUNT on NET_SF_JMONEY_BANKACCOUNT.\"_ID\" = NET_SF_JMONEY_ACCOUNT.\"_ID\"";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_BANKACCOUNT";
	}
	
	@Override
	public String getColumnSelectors() {
		// The accounting tables may self-join (to get parent), so qualify all columns with the table id.
		return capitalAccountTable.getColumnSelectors() + ", NET_SF_JMONEY_BANKACCOUNT.\"net_sf_jmoney_jmfinn_jmFinnAccount_finnAccountName\", NET_SF_JMONEY_BANKACCOUNT.\"minBalance\", NET_SF_JMONEY_BANKACCOUNT.\"accountNumber\", NET_SF_JMONEY_BANKACCOUNT.\"bank\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "ACCOUNT_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		/* 
		 * We don't return the decimal places here because all amounts are converted from long
		 * to decimal by this driver.  The number of decimal places is really used for conversion
		 * of money amounts to pennies for internal storage of integer values.  Possibly consumers
		 * could use this information for formatting or rounding purposes but that is unlikely.
		 */
		
		int id = result.getInt("ACCOUNT_ID");
		String name = result.getString("ACCOUNT_NAME");

		String comment = result.getString("comment");
		String abbreviation = result.getString("abbreviation");
		
		long minBalance = result.getLong("minBalance");
		BigDecimal minBalanceAsDecimal = new BigDecimal(minBalance).scaleByPowerOfTen(-2);
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Name", name))
				.addProperty(createPrimitive("Comment", comment))
				.addProperty(createPrimitive("Abbreviation", abbreviation))
				.addProperty(createPrimitive("JMFinnName", result.getString("net_sf_jmoney_jmfinn_jmFinnAccount_finnAccountName")))
				.addProperty(createPrimitive("MinimumBalance", minBalanceAsDecimal))
				.addProperty(createPrimitive("AccountNumber", result.getString("accountNumber")))
				.addProperty(createPrimitive("Bank", result.getString("bank")));
				
		entity.setId(createId(AccountsEdmProvider.ES_BANK_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_BANK_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("ACCOUNT_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_BANK_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_BANK_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "ParentAccount":
			return capitalAccountTable.readParentAccountExpectingEntity(uriInfo, resourceParts, index, key, expandOption);
		}

		return null; // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			return accountTable.readEntries(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "ParentAccount":
			return capitalAccountTable.readParentAccount(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "SubAccounts":
			return capitalAccountTable.readSubAccounts(uriInfo, resourceParts, index, key, filterOption, expandOption);
		}
		
		throw new ODataApplicationException("Unexpected navigation from BankAccount: " + navProp.getName(), 1, Locale.ENGLISH);
	}

}
