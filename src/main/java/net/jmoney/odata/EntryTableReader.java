/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.Constants;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Link;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class EntryTableReader extends TableReader {

	private TableReader transactionTable;
	private TableReader accountTable;
	private TableReader commodityTable;
	
	private PreparedStatement lookupById;

	private PreparedStatement transactionForEntry;
	private PreparedStatement accountForEntry;
	private PreparedStatement commodityForEntry;

	public EntryTableReader(Connection connection) throws SQLException {
		super(connection);
		
		properties.put("Amount", new AccountingProperty("\"amount\"", Type.INTEGER));
		properties.put("Memo", new AccountingProperty("\"memo\"", Type.STRING));
		properties.put("Check", new AccountingProperty("\"check\"", Type.STRING));
		properties.put("Valuta", new AccountingProperty("\"valuta\"", Type.DATE));
	}

	@Override
	public void init(TableReaderFactory readerFactory) throws SQLException {
		transactionTable = readerFactory.getTransactionTableReader();
		accountTable = readerFactory.getAccountTableReader();
		commodityTable = readerFactory.getCommodityTableReader();
		
		lookupById = connection.prepareStatement("select " + getColumnSelectors() +" from " + getTableJoins() + " where \"_ID\" = ?");
		transactionForEntry = connection.prepareStatement("select " + transactionTable.getColumnSelectors() + " from " + getTableJoins() + " join NET_SF_JMONEY_TRANSACTION on NET_SF_JMONEY_ENTRY.\"net_sf_jmoney_transaction_entry\" = NET_SF_JMONEY_TRANSACTION.\"_ID\" where NET_SF_JMONEY_ENTRY.\"_ID\" = ?");
		accountForEntry = connection.prepareStatement("select " + accountTable.getColumnSelectors() + " from " + getTableJoins() + " join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_ENTRY.\"account\" = NET_SF_JMONEY_ACCOUNT.\"_ID\" where NET_SF_JMONEY_ENTRY.\"_ID\" = ?");
		commodityForEntry = connection.prepareStatement("select " + commodityTable.getColumnSelectors() + " from " + getTableJoins() + " join NET_SF_JMONEY_COMMODITY on NET_SF_JMONEY_ENTRY.\"commodity\" = NET_SF_JMONEY_COMMODITY.\"_ID\" where NET_SF_JMONEY_ENTRY.\"_ID\" = ?");

		navigationProperties.put("Transaction",
				new ExpandedItem("Transaction", transactionTable, "\"net_sf_jmoney_transaction_entry\""));
		navigationProperties.put("Account",
				new ExpandedItem("Account", accountTable, "\"account\""));
		navigationProperties.put("Commodity",
				new ExpandedItem("Commodity", commodityTable, "\"commodity\""));
	}
	
	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_ENTRY";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_ENTRY";
	}
	
	@Override
	public String getColumnSelectors() {
		return "NET_SF_JMONEY_ENTRY.\"_ID\" as ENTRY_ID, \"creation\", \"amount\", \"valuta\", \"memo\", \"check\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "ENTRY_ID";
	}

	@Override
	protected ResultSet executeQuery(Statement stmt, FilterOption filterOption, List<Expansion> expandedItems) throws SQLException, ODataApplicationException {
		String columnsPartOfQuery = getColumnSelectors();
		String tablesPartOfQuery = getTableJoins();

		for (Expansion expansion : expandedItems) {
			ExpandedItem expandedItem = expansion.navigationProperty;
			if (expansion.isIdOnly) {
				columnsPartOfQuery += ", NET_SF_JMONEY_ENTRY." + expandedItem.foreignKeyName + " as " + expandedItem.getReader().getIdAlias();
			} else {
				tablesPartOfQuery += " left outer join " + expandedItem.getReader().getTableName() + " on NET_SF_JMONEY_ENTRY." + expandedItem.foreignKeyName + " = " + expandedItem.getReader().getTableName() + ".\"_ID\"";
			}
		}
		
		String basicQuery = "select " + columnsPartOfQuery + " from " + tablesPartOfQuery;
		
		if (filterOption != null) {
			String whereClause = buildWhereClause(filterOption);
			return stmt.executeQuery(basicQuery + " where " + whereClause);
		} else {
			return stmt.executeQuery(basicQuery);
		}
	}

	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		int id = result.getInt("ENTRY_ID");
		long creation = result.getLong("creation");
		long amount = result.getLong("amount");
		String memo = result.getString("memo");
		Date valuta = result.getDate("valuta");
		String checkNumber = result.getString("check");
		
		Date creationDate = new Date(creation);
		BigDecimal amount2 = new BigDecimal(amount).scaleByPowerOfTen(-2);
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Valuta", valuta))
				.addProperty(createPrimitive("Memo", memo))
				.addProperty(createPrimitive("Amount", amount2))
				.addProperty(createPrimitive("Creation", creationDate))
				.addProperty(createPrimitive("CheckNumber", checkNumber));

		if (memo != null) {
			String [] parts = memo.split(" \\- ", 3);
			for (int index = 0; index < parts.length; index++) {
				String propertyName = "Group" + (index+1);
				entity.addProperty(createPrimitive(propertyName, parts[index]));
			}
		}

		entity.setId(createId(AccountsEdmProvider.ES_ENTRIES_NAME, id));
		entity.setType(AccountsEdmProvider.ET_ENTRY.getFullQualifiedNameAsString());
		for (Expansion expansion : expandedItems) {
			ExpandedItem expandedItem = expansion.navigationProperty; 
			TableReader readerForExpandedItem = expandedItem.getReader();
			
			Entity expandEntity;
			if (expansion.isIdOnly) {
				expandEntity = readerForExpandedItem.createEntityFromResultWithIdOnly(result);
			} else {
				// Currently nested expanded items are not supported so we pass on the empty list
				expandEntity = readerForExpandedItem.createEntityFromResult(result, Collections.<Expansion>emptyList());
			}
			
			Link link = new Link();
			link.setTitle(expandedItem.getNavigationPropertyName());
	        link.setType(Constants.ENTITY_NAVIGATION_LINK_TYPE);
	        link.setRel(Constants.NS_ASSOCIATION_LINK_REL + expandedItem.getNavigationPropertyName());
			link.setInlineEntity(expandEntity);
            link.setHref(expandEntity.getId().toASCIIString());
			entity.getNavigationLinks().add(link);
		}
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("ENTRY_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_ENTRIES_NAME, id));
		entity.setType(AccountsEdmProvider.ET_ENTRY.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, Collections.<Expansion>emptyList());
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = this.buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Transaction":
			transactionForEntry.setInt(1, key);
			ResultSet transactionResult = transactionForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
				transactionResult.next();
				List<Expansion> expandedItems = this.buildExpandedItems(expandOption);
				Entity el = transactionTable.createEntityFromResult(transactionResult, expandedItems);
				return el;
			} else {
				// There are more parts to join
				transactionResult.next();
				int transactionId = transactionResult.getInt("_ID");
				Entity entityForThisTransaction = transactionTable.readAndJoin(uriInfo, resourceParts, index + 1, transactionId, expandOption); 
				return entityForThisTransaction;
			}

		case "Account":
			accountForEntry.setInt(1, key);
			ResultSet accountResult = accountForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
				accountResult.next();
				List<Expansion> expandedItems = this.buildExpandedItems(expandOption);
				Entity el = accountTable.createEntityFromResult(accountResult, expandedItems);
				return el;
			} else {
				// There are more parts to join
				accountResult.next();
				int transactionId = accountResult.getInt("_ID");
				Entity entityForThisAccount = accountTable.readAndJoin(uriInfo, resourceParts, index + 1, transactionId, expandOption); 
				return entityForThisAccount;
			}

		case "Commodity":
			CommodityTableReader commodityTable = new CommodityTableReader(connection);

			commodityForEntry.setInt(1, key);
			ResultSet commodityResult = commodityForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
				commodityResult.next();
				List<Expansion> expandedItems = this.buildExpandedItems(expandOption);
				Entity el = commodityTable.createEntityFromResult(commodityResult, expandedItems);
				return el;
			} else {
				// There are more parts to join
				commodityResult.next();
				int transactionId = commodityResult.getInt("_ID");
				Entity entityForThisCommodity = commodityTable.readAndJoin(uriInfo, resourceParts, index + 1, transactionId, expandOption); 
				return entityForThisCommodity;
			}

		}
		return null; // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
//			// We are done with the joins, so this is it.
//			Entity thisOne = read(key);
//			return thisOne;
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Transaction":
			transactionForEntry.setInt(1, key);
			ResultSet transactionResult = transactionForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// We are done with the joins but we expected to end with a collection so we fail.
				throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
			} else {
				// There are more parts to join
				transactionResult.next();
				int transactionId = transactionResult.getInt("_ID");
				EntityCollection entitiesForThisTransaction = transactionTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, transactionId, filterOption, expandOption); 
				return entitiesForThisTransaction;
			}

		case "Account":
			accountForEntry.setInt(1, key);
			ResultSet accountResult = accountForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
//				accountResult.next();
//				Entity el = accountTable.createEntityFromResult(accountResult, Collections.emptyList());
//				return el;
				// We are done with the joins but we expected to end with a collection so we fail.
				throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
			} else {
				// There are more parts to join
				accountResult.next();
				int transactionId = accountResult.getInt("_ID");
				EntityCollection entitiesForThisAccount = accountTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, transactionId, filterOption, expandOption); 
				return entitiesForThisAccount;
			}

		case "Commodity":
			commodityForEntry.setInt(1, key);
			ResultSet commodityResult = commodityForEntry.executeQuery();

			if (index + 2 == resourceParts.size()) {
				// This is the last part, so return this result set as entities
//				commodityResult.next();
//				Entity el = commodityTable.createEntityFromResult(commodityResult);
//				return el;
				// We are done with the joins but we expected to end with a collection so we fail.
				throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
			} else {
				// There are more parts to join
				commodityResult.next();
				int transactionId = commodityResult.getInt("_ID");
				EntityCollection entitiesForThisCommodity = commodityTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, transactionId, filterOption, expandOption); 
				return entitiesForThisCommodity;
			}

		}
		return null; // TODO
	}
}
