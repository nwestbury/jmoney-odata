package net.jmoney.odata;

import net.jmoney.odata.WherePart.Type;

public class AccountingProperty {

	String sqlName;
	String unquotedSqlName;
	Type type;
	
	public AccountingProperty(String sqlName, Type type) {
		this.sqlName = sqlName;
		this.type = type;
		
		if (sqlName.startsWith("\"")) {
			unquotedSqlName = sqlName.substring(1, sqlName.length()-1);
		} else {
			unquotedSqlName = sqlName;
		}
	}

	public WherePart getWherePart() {
		return new WherePart(sqlName, type);
	}

}
