/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.commons.api.edm.EdmProperty;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.CountOption;
import org.apache.olingo.server.api.uri.queryoption.ExpandItem;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.SelectItem;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.apache.olingo.server.api.uri.queryoption.SkipOption;
import org.apache.olingo.server.api.uri.queryoption.TopOption;
import org.apache.olingo.server.api.uri.queryoption.expression.Expression;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitException;

import net.jmoney.odata.DataProvider.DataProviderException;
import net.jmoney.odata.WherePart.Type;

public abstract class TableReader {

	protected Connection connection;
	
	protected Map<String, AccountingProperty> properties = new HashMap<>();

	protected Map<String, ExpandedItem> navigationProperties = new HashMap<>();
	
	protected Map<String, ExpandedCollection> navigationCollectionProperties = new HashMap<>();
	
	protected TableReader(Connection connection) {
		this.connection = connection;
		properties.put("Id", new AccountingProperty("\"_ID\"", Type.INTEGER));
	}

	protected abstract void init(TableReaderFactory readerFactory) throws SQLException;
	
	public EntityCollection readAll(UriInfo uriInfo, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		// Create a statement for our use.
		try (Statement stmt = connection.createStatement()) {
			EntityCollection entitySet = new EntityCollection();

			int count = Integer.MAX_VALUE;
			CountOption countOption = uriInfo.getCountOption();
			if (countOption != null) {
				boolean isCount = countOption.getValue();
				if (isCount) {
					count = fetchCount(stmt, filterOption);
					entitySet.setCount(count);
				}
			}

			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			
			ResultSet result = executeQuery(stmt, filterOption, expandedItems);

			SkipOption skipOption = uriInfo.getSkipOption();
			if (skipOption != null) {
				int skipNumber = skipOption.getValue();
				if (skipNumber >= 0) {
					if(skipNumber <= count) {
						while (skipNumber > 0 && result.next()) {
							skipNumber--;
						}
					} else {
						// All entries are being skipped
						stmt.close();
						return entitySet;
					}
				} else {
					throw new ODataApplicationException("Invalid value for $skip", HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ROOT);
				}
			}

			int topNumber = Integer.MAX_VALUE;
			TopOption topOption = uriInfo.getTopOption();
			if (topOption != null) {
				topNumber = topOption.getValue();
				if (topNumber < 0) {
					throw new ODataApplicationException("Invalid value for $top", HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ROOT);
				}
			}
			while (topNumber > 0 && result.next()) {
				Entity el = createEntityFromResult(result, expandedItems);
				entitySet.getEntities().add(el);
				topNumber--;
			}
			return entitySet;
		}
	}

	public List<Expansion> buildExpandedItems(ExpandOption expandOption) {
		List<Expansion> expandedItems = new ArrayList<>();
		if (expandOption != null) {
			for (ExpandItem expandItem : expandOption.getExpandItems()) {
				if (expandItem.isStar()) {
					navigationProperties.values().forEach(navigationProperty -> {
						Expansion expansion = new Expansion(navigationProperty, false);
						expandedItems.add(expansion);
					});
				} else {
					// Nothing is selected except the id.  As an optimization we avoid doing a join
					// to the next table altogether and get the id from the foreign key column in the
					// previous table.
					SelectOption selectOption = expandItem.getSelectOption();
					boolean isIdOnly = selectOption != null
							&& selectOption.getSelectItems().stream().allMatch(item -> isJustTheId(item));

					// can be 'Id', 'Name' etc, no path supported
					UriResource uriResource = expandItem.getResourcePath().getUriResourceParts().get(0);
					// we don't need to handle error cases, as it is done in the Olingo library
					if (uriResource instanceof UriResourceNavigation) {
						EdmNavigationProperty edmNavigationProperty = ((UriResourceNavigation) uriResource).getProperty();
						ExpandedItem navigationProperty = navigationProperties.get(edmNavigationProperty.getName());
						// Will be null if the property is a collection, not an entity
						if (navigationProperty != null) {
							Expansion expansion = new Expansion(navigationProperty, isIdOnly);
							expandedItems.add(expansion);
						}
					}
				}
			}
		}
		return expandedItems;
	}

	public List<ExpandedCollection> buildExpandedCollections(ExpandOption expandOption) {
		List<ExpandedCollection> expandedItems = new ArrayList<>();
		if (expandOption != null) {
			for (ExpandItem expandItem : expandOption.getExpandItems()) {
				if (expandItem.isStar()) {
					expandedItems.addAll(navigationCollectionProperties.values());
				} else {
					// can be 'Id', 'Name' etc, no path supported
					UriResource uriResource = expandItem.getResourcePath().getUriResourceParts().get(0);
					// we don't need to handle error cases, as it is done in the Olingo library
					if (uriResource instanceof UriResourceNavigation) {
						EdmNavigationProperty edmNavigationProperty = ((UriResourceNavigation) uriResource).getProperty();
						ExpandedCollection navigationProperty = navigationCollectionProperties.get(edmNavigationProperty.getName());
						// Will be null if the property is an entity, not a collection
						if (navigationProperty != null) {
							expandedItems.add(navigationProperty);
						}
					}
				}
			}
		}
		return expandedItems;
	}

	private boolean isJustTheId(SelectItem item) {
		List<UriResource> parts = item.getResourcePath().getUriResourceParts();
		return parts.size() == 1 && parts.get(0).getSegmentValue().equals("Id");
	}

	protected URI createId(String entitySetName, Object id) {
		try {
			return new URI(entitySetName + "(" + String.valueOf(id) + ")");
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create id for entity: " + entitySetName, e);
		}
	}
	
	protected Property createPrimitive(final String name, final Object value) {
		return new Property(null, name, ValueType.PRIMITIVE, value);
	}
	
	protected ResultSet executeQuery(Statement stmt, FilterOption filterOption, List<Expansion> expandedItems) throws SQLException, ODataApplicationException {
		String basicQuery = "select " + getColumnSelectors() + " from " + getTableJoins();
		if (filterOption != null) {
			String whereClause = buildWhereClause(filterOption);
			return stmt.executeQuery(basicQuery + " where " + whereClause);
		} else {
			return stmt.executeQuery(basicQuery);
		}
	}
	
	private int fetchCount(Statement stmt, FilterOption filterOption) throws SQLException, ODataApplicationException {
		ResultSet result;
		
		if (filterOption != null) {
			// As there is a 'where' clause, we need to join all the tables because
			// columns from other tables could be referenced.
			String basicQuery = "select count(*) from " + getTableJoins();
			String whereClause = buildWhereClause(filterOption);
			result = stmt.executeQuery(basicQuery + " where " + whereClause);
		} else {
			// No need to join tables here.  Just count in the table itself.
			String basicQuery = "select count(*) from " + getTableName();
			result = stmt.executeQuery(basicQuery);
		}

		result.next();
		int count = result.getInt(1);
		return count;
	}
	
	protected abstract Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException;
	protected abstract Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException;
	protected abstract Entity read(int id, List<Expansion> expandedItems) throws SQLException;

	/**
	 * Reads all entries from this table, then joins to the remaining tables
	 * @param uriInfo
	 * @param resourceParts
	 * @param index the index of this table in resourceParts
	 * @return
	 * @throws DataProviderException 
	 * @throws SQLException 
	 */
	public Entity readAndJoin(UriInfo uriInfo, List<UriResource> resourceParts, int index) throws DataProviderException, SQLException {
		// Get the initial keyset
		UriResource thisResource = resourceParts.get(index);
		final UriResourceEntitySet uriResource = (UriResourceEntitySet) thisResource;
		List<UriParameter> keys = uriResource.getKeyPredicates();
		if (keys.size() != 1) {
			throw new DataProviderException("table must have one key field: " + this.toString());
		}
		String keyValueText = keys.get(0).getText();
		int keyValue = Integer.valueOf(keyValueText);
	
		return readAndJoin2(uriInfo, resourceParts, index, keyValue, uriInfo.getExpandOption());
	}
	
	public Entity readAndJoin(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		return readAndJoin2(uriInfo, resourceParts, index, key, expandOption);
	}

	public EntityCollection readAndJoinCollection(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws DataProviderException, SQLException, ODataApplicationException {
		// Get the initial keyset
		UriResource thisResource = resourceParts.get(0);
		final UriResourceEntitySet uriResource = (UriResourceEntitySet) thisResource;
		List<UriParameter> keys = uriResource.getKeyPredicates();
		if (keys.size() == 0) {
			return readAndJoinCollection2(uriInfo, resourceParts, filterOption, expandOption);
		}
		if (keys.size() != 1) {
			throw new DataProviderException("table must have one key field: " + this.toString());
		}
		String keyValueText = keys.get(0).getText();
		int keyValue = Integer.valueOf(keyValueText);
	
		return readAndJoinCollection2(uriInfo, resourceParts, 0, keyValue, filterOption, expandOption);
	}
	
	public EntityCollection readAndJoinCollection(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		return readAndJoinCollection2(uriInfo, resourceParts, index, key, filterOption, expandOption);
	}
	
	public abstract Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException;

	public abstract EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException;
	
	public abstract EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException;

	public WherePart getSqlForProperty(EdmProperty property) {
		AccountingProperty accountingProp = properties.get(property.getName());
		return accountingProp.getWherePart();
	}

	/**
	 * 
	 * @param filterOption must not be null
	 * @return a SQL boolean expression, never null or empty
	 * @throws ODataApplicationException
	 */
	protected String buildWhereClause(FilterOption filterOption) throws ODataApplicationException {
		try {
			Expression filterExpression = filterOption.getExpression();
			FilterExpressionToWhereClause expressionVisitor = new FilterExpressionToWhereClause(this);

			// Evaluating the expression
			WherePart visitorResult = filterExpression.accept(expressionVisitor);

			String whereClause = visitorResult.getSql();

			// The result of the filter expression must be of type String
			System.out.println("where clause is " + visitorResult.getSql());

			return whereClause;
		} catch (ExpressionVisitException e) {
			e.printStackTrace();
			throw new ODataApplicationException("SQLException", 500, Locale.ENGLISH, e);
		}		
	}

	public abstract String getTableJoins();

	public abstract String getColumnSelectors();
	
	public abstract String getTableName();

	public abstract String getIdAlias();

}
