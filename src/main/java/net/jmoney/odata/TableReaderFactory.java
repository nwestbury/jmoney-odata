/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * We have two-part construction here because we must assign the constructed
 * table reader to its field before calling code that may recursively request
 * the same table reader.
 * 
 * The init code can assume that the referenced tables have been constructed but
 * cannot assume their init() method has been called.  All other methods can assume
 * that the init() method of all referenced tables has been called.
 */
public class TableReaderFactory {

	private Connection connection;
	
	private TableReader entryTableReader = null;
	private TableReader transactionTableReader = null;
	private AccountTableReader accountTableReader = null;
	private IncomeExpenseAccountTable incomeExpenseAccountTable = null;
	private CapitalAccountTable capitalAccountTable = null;
	private BankAccountTable bankAccountTable = null;
	private StockAccountTable stockAccountTable = null;
	private TableReader commodityTableReader = null;
	private TableReader currencyTableReader = null;
	
	public TableReaderFactory(Connection connection) {
		this.connection = connection;
	}
	
	public TableReader getEntryTableReader() throws SQLException {
		if (entryTableReader == null) {
			entryTableReader = new EntryTableReader(connection);
			entryTableReader.init(this);
		}
		return entryTableReader;
	}

	public TableReader getTransactionTableReader() throws SQLException {
		if (transactionTableReader == null) {
			transactionTableReader = new TransactionTableReader(connection);
			transactionTableReader.init(this);
		}
		return transactionTableReader;
	}

	public AccountTableReader getAccountTableReader() throws SQLException {
		if (accountTableReader == null) {
			accountTableReader = new AccountTableReader(connection);
			accountTableReader.init(this);
		}
		return accountTableReader;
	}

	public IncomeExpenseAccountTable getIncomeExpenseAccountTable() throws SQLException {
		if (incomeExpenseAccountTable == null) {
			incomeExpenseAccountTable = new IncomeExpenseAccountTable(connection);
			incomeExpenseAccountTable.init(this);
		}
		return incomeExpenseAccountTable;
	}

	public CapitalAccountTable getCapitalAccountTable() throws SQLException {
		if (capitalAccountTable == null) {
			capitalAccountTable = new CapitalAccountTable(connection);
			capitalAccountTable.init(this);
		}
		return capitalAccountTable;
	}

	public BankAccountTable getBankAccountTable() throws SQLException {
		if (bankAccountTable == null) {
			bankAccountTable = new BankAccountTable(connection);
			bankAccountTable.init(this);
		}
		return bankAccountTable;
	}

	public StockAccountTable getStockAccountTable() throws SQLException {
		if (stockAccountTable == null) {
			stockAccountTable = new StockAccountTable(connection);
			stockAccountTable.init(this);
		}
		return stockAccountTable;
	}

	public TableReader getCommodityTableReader() throws SQLException {
		if (commodityTableReader == null) {
			commodityTableReader = new CommodityTableReader(connection);
			commodityTableReader.init(this);
		}
		return commodityTableReader;
	}

	public TableReader getCurrencyTableReader() throws SQLException {
		if (currencyTableReader == null) {
			currencyTableReader = new CurrencyTableReader(connection);
			currencyTableReader.init(this);
		}
		return currencyTableReader;
	}

}
