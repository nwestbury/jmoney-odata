/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;
import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.apache.olingo.commons.api.edm.provider.CsdlAbstractEdmProvider;
import org.apache.olingo.commons.api.edm.provider.CsdlComplexType;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainer;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainerInfo;
import org.apache.olingo.commons.api.edm.provider.CsdlEntitySet;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityType;
import org.apache.olingo.commons.api.edm.provider.CsdlNavigationProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlNavigationPropertyBinding;
import org.apache.olingo.commons.api.edm.provider.CsdlProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlPropertyRef;
import org.apache.olingo.commons.api.edm.provider.CsdlSchema;
import org.apache.olingo.commons.api.ex.ODataException;

public class AccountsEdmProvider extends CsdlAbstractEdmProvider {

	// Service Namespace
	public static final String NAMESPACE = "olingo.odata.sample";

	// EDM Container
	public static final String CONTAINER_NAME = "Container";
	public static final FullQualifiedName CONTAINER_FQN = new FullQualifiedName(NAMESPACE, CONTAINER_NAME);

	// Entity Types Names
	public static final FullQualifiedName ET_ENTRY = new FullQualifiedName(NAMESPACE, "Entry");
	public static final FullQualifiedName ET_TRANSACTION = new FullQualifiedName(NAMESPACE, "Transaction");
	public static final FullQualifiedName ET_COMMODITY = new FullQualifiedName(NAMESPACE, "Commodity");
	public static final FullQualifiedName ET_CURRENCY = new FullQualifiedName(NAMESPACE, "Currency");
	public static final FullQualifiedName ET_ACCOUNT = new FullQualifiedName(NAMESPACE, "Account");
	public static final FullQualifiedName ET_INCOME_EXPENSE_ACCOUNT = new FullQualifiedName(NAMESPACE, "Category");
	public static final FullQualifiedName ET_CAPITAL_ACCOUNT = new FullQualifiedName(NAMESPACE, "CapitalAccount");
	public static final FullQualifiedName ET_BANK_ACCOUNT = new FullQualifiedName(NAMESPACE, "BankAccount");
	public static final FullQualifiedName ET_STOCK_ACCOUNT = new FullQualifiedName(NAMESPACE, "StockAccount");

	// Complex Type Names
	public static final FullQualifiedName CT_ADDRESS = new FullQualifiedName(NAMESPACE, "Address");

	// Entity Set Names
	public static final String ES_ENTRIES_NAME = "Entries";
	public static final String ES_TRANSACTIONS_NAME = "Transactions";
	public static final String ES_COMMODITIES_NAME = "Commodities";
	public static final String ES_CURRENCIES_NAME = "Currencies";
	public static final String ES_ACCOUNTS_NAME = "Accounts";
	public static final String ES_INCOME_EXPENSE_ACCOUNTS_NAME = "Categories";
	public static final String ES_CAPITAL_ACCOUNTS_NAME = "CapitalAccounts";
	public static final String ES_BANK_ACCOUNTS_NAME = "BankAccounts";
	public static final String ES_STOCK_ACCOUNTS_NAME = "StockAccounts";

	@Override
	public List<CsdlSchema> getSchemas() throws ODataException {
		List<CsdlSchema> schemas = new ArrayList<CsdlSchema>();
		CsdlSchema schema = new CsdlSchema();
		schema.setNamespace(NAMESPACE);
		// EntityTypes
		List<CsdlEntityType> entityTypes = new ArrayList<CsdlEntityType>();
		entityTypes.add(getEntityType(ET_ENTRY));
		entityTypes.add(getEntityType(ET_TRANSACTION));
		entityTypes.add(getEntityType(ET_ACCOUNT));
		entityTypes.add(getEntityType(ET_INCOME_EXPENSE_ACCOUNT));
		entityTypes.add(getEntityType(ET_CAPITAL_ACCOUNT));
		entityTypes.add(getEntityType(ET_BANK_ACCOUNT));
		entityTypes.add(getEntityType(ET_STOCK_ACCOUNT));
		entityTypes.add(getEntityType(ET_COMMODITY));
		entityTypes.add(getEntityType(ET_CURRENCY));
		schema.setEntityTypes(entityTypes);

		// ComplexTypes
		List<CsdlComplexType> complexTypes = new ArrayList<CsdlComplexType>();
		complexTypes.add(getComplexType(CT_ADDRESS));
		schema.setComplexTypes(complexTypes);

		// EntityContainer
		schema.setEntityContainer(getEntityContainer());
		schemas.add(schema);

		return schemas;
	}
	
	@Override
	public CsdlComplexType getComplexType(final FullQualifiedName complexTypeName) throws ODataException {
		if (CT_ADDRESS.equals(complexTypeName)) {
			return new CsdlComplexType().setName(CT_ADDRESS.getName()).setProperties(Arrays.asList(
					new CsdlProperty().setName("Street").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
					new CsdlProperty().setName("City").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
					new CsdlProperty().setName("ZipCode").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
					new CsdlProperty().setName("Country").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					));
		}
		return null;
	}
	
	@Override
	public CsdlEntityType getEntityType(final FullQualifiedName entityTypeName) throws ODataException {
		final String entriesInTransactionPropName = "Entries";
		final String entriesInAccountPropName = "Entries";
		final String subAccountsInIncomeExpenseAccountPropName = "SubAccounts";
		final String subAccountsInCapitalAccountPropName = "SubAccounts";
		
		if (ET_ACCOUNT.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_ACCOUNT.getName())
					.setKey(Arrays.asList(new CsdlPropertyRef().setName("Id")))
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Id").setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName()),
							new CsdlProperty().setName("Name").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					))
					.setNavigationProperties(Arrays.asList(
							new CsdlNavigationProperty().setName(entriesInAccountPropName).setType(ET_ENTRY).setCollection(true).setPartner("Account")
					));
		} else if (ET_INCOME_EXPENSE_ACCOUNT.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_INCOME_EXPENSE_ACCOUNT.getName())
					.setBaseType(ET_ACCOUNT)
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("TaxCategory").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					))
					.setNavigationProperties(Arrays.asList(
							new CsdlNavigationProperty().setName("ParentAccount").setType(ET_INCOME_EXPENSE_ACCOUNT).setPartner(subAccountsInIncomeExpenseAccountPropName),
							new CsdlNavigationProperty().setName(subAccountsInIncomeExpenseAccountPropName).setType(ET_INCOME_EXPENSE_ACCOUNT).setCollection(true).setPartner("ParentAccount")
					));
		} else if (ET_CAPITAL_ACCOUNT.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_CAPITAL_ACCOUNT.getName())
					.setBaseType(ET_ACCOUNT)
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Comment").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Abbreviation").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					))
					.setNavigationProperties(Arrays.asList(
							new CsdlNavigationProperty().setName("ParentAccount").setType(ET_CAPITAL_ACCOUNT).setPartner(subAccountsInCapitalAccountPropName),
							new CsdlNavigationProperty().setName(subAccountsInCapitalAccountPropName).setType(ET_CAPITAL_ACCOUNT).setCollection(true).setPartner("ParentAccount")
					));
		} else if (ET_BANK_ACCOUNT.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_BANK_ACCOUNT.getName())
					.setBaseType(ET_CAPITAL_ACCOUNT)
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("JMFinnName").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("MinimumBalance").setType(EdmPrimitiveTypeKind.Double.getFullQualifiedName()),
							new CsdlProperty().setName("AccountNumber").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Bank").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					));
		} else if (ET_STOCK_ACCOUNT.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_STOCK_ACCOUNT.getName())
					.setBaseType(ET_CAPITAL_ACCOUNT)
					.setProperties(StockAccountTable.myProperties.entrySet().stream().map(
							(Map.Entry<String, AccountingProperty> keyValuePair) -> {
								return new CsdlProperty().setName(keyValuePair.getKey()).setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
							}).collect(Collectors.toList()));
		} else if (ET_ENTRY.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_ENTRY.getName())
					.setKey(Arrays.asList(new CsdlPropertyRef().setName("Id")))
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Id").setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName()),
							new CsdlProperty().setName("Memo").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Group1").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Group2").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Group3").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Amount").setType(EdmPrimitiveTypeKind.Double.getFullQualifiedName()),
							new CsdlProperty().setName("Valuta").setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName()),
							new CsdlProperty().setName("CheckNumber").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()),
							new CsdlProperty().setName("Creation").setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName())
					))
					.setNavigationProperties(Arrays.asList(
							new CsdlNavigationProperty().setName("Account").setType(ET_ACCOUNT).setNullable(false).setPartner(entriesInAccountPropName),
							new CsdlNavigationProperty().setName("Transaction").setType(ET_TRANSACTION).setNullable(false).setPartner(entriesInTransactionPropName),
							new CsdlNavigationProperty().setName("Commodity").setType(ET_COMMODITY).setNullable(false)
					));
		} else if (ET_TRANSACTION.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_TRANSACTION.getName())
					.setKey(Arrays.asList(new CsdlPropertyRef().setName("Id")))
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Id").setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName()),
							new CsdlProperty().setName("Date").setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName())
					))
					.setNavigationProperties(Arrays.asList(
							new CsdlNavigationProperty().setName(entriesInTransactionPropName).setType(ET_ENTRY).setCollection(true).setPartner("Transaction")
					));
		} else if (ET_COMMODITY.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_COMMODITY.getName())
					.setKey(Arrays.asList(new CsdlPropertyRef().setName("Id")))
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Id").setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName()),
							new CsdlProperty().setName("Name").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					));
		} else if (ET_CURRENCY.equals(entityTypeName)) {
			return new CsdlEntityType()
					.setName(ET_CURRENCY.getName())
					.setBaseType(ET_COMMODITY)
					.setProperties(Arrays.asList(
							new CsdlProperty().setName("Code").setType(EdmPrimitiveTypeKind.String.getFullQualifiedName())
					));
		}

		return null;
	}

	@Override
	public CsdlEntitySet getEntitySet(final FullQualifiedName entityContainer, final String entitySetName) throws ODataException {
		if (CONTAINER_FQN.equals(entityContainer)) {
			if (ES_ACCOUNTS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_ACCOUNTS_NAME)
						.setType(ET_ACCOUNT)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME)
										)
								);
			} else if (ES_INCOME_EXPENSE_ACCOUNTS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_INCOME_EXPENSE_ACCOUNTS_NAME)
						.setType(ET_INCOME_EXPENSE_ACCOUNT)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("ParentAccount")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_INCOME_EXPENSE_ACCOUNTS_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("SubAccounts")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_INCOME_EXPENSE_ACCOUNTS_NAME)
										)
								);
			} else if (ES_CAPITAL_ACCOUNTS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_CAPITAL_ACCOUNTS_NAME)
						.setType(ET_CAPITAL_ACCOUNT)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("ParentAccount")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("SubAccounts")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME)
										)
								);
			} else if (ES_BANK_ACCOUNTS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_BANK_ACCOUNTS_NAME)
						.setType(ET_BANK_ACCOUNT)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("ParentAccount")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("SubAccounts")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME)
										)
								);
			} else if (ES_STOCK_ACCOUNTS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_STOCK_ACCOUNTS_NAME)
						.setType(ET_STOCK_ACCOUNT)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("ParentAccount")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME),
										new CsdlNavigationPropertyBinding()
										.setPath("SubAccounts")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_CAPITAL_ACCOUNTS_NAME)
										)
								);
			} else if (ES_ENTRIES_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_ENTRIES_NAME)
						.setType(ET_ENTRY)
						.setNavigationPropertyBindings(
							Arrays.asList(
								new CsdlNavigationPropertyBinding()
									.setPath("Account")
									.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ACCOUNTS_NAME),
								new CsdlNavigationPropertyBinding()
									.setPath("Transaction")
									.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_TRANSACTIONS_NAME),
								new CsdlNavigationPropertyBinding()
									.setPath("Commodity")
									.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_COMMODITIES_NAME)
							)
						);
			} else if (ES_TRANSACTIONS_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_TRANSACTIONS_NAME)
						.setType(ET_TRANSACTION)
						.setNavigationPropertyBindings(
								Arrays.asList(
										new CsdlNavigationPropertyBinding()
										.setPath("Entries")
										.setTarget(CONTAINER_FQN.getFullQualifiedNameAsString() + "/" + ES_ENTRIES_NAME)
										)
								);
			} else if (ES_COMMODITIES_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_COMMODITIES_NAME)
						.setType(ET_COMMODITY);
			} else if (ES_CURRENCIES_NAME.equals(entitySetName)) {
				return new CsdlEntitySet()
						.setName(ES_CURRENCIES_NAME)
						.setType(ET_CURRENCY);
			}
		}

		return null;
	}

	@Override
	public CsdlEntityContainer getEntityContainer() throws ODataException {
		CsdlEntityContainer container = new CsdlEntityContainer();
		container.setName(CONTAINER_FQN.getName());

		// EntitySets
		List<CsdlEntitySet> entitySets = new ArrayList<CsdlEntitySet>();
		container.setEntitySets(entitySets);
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_ENTRIES_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_TRANSACTIONS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_INCOME_EXPENSE_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_CAPITAL_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_BANK_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_STOCK_ACCOUNTS_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_COMMODITIES_NAME));
		entitySets.add(getEntitySet(CONTAINER_FQN, ES_CURRENCIES_NAME));

		return container;
	}
	
	@Override
	public CsdlEntityContainerInfo getEntityContainerInfo(final FullQualifiedName entityContainerName)
			throws ODataException {
		if (entityContainerName == null || CONTAINER_FQN.equals(entityContainerName)) {
			return new CsdlEntityContainerInfo().setContainerName(CONTAINER_FQN);
		}
		return null;
	}
}