/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class CapitalAccountTable extends TableReader {

	private AccountTableReader accountTable;
	
	private PreparedStatement lookupById;

	private PreparedStatement subAccountsByAccount;

	private PreparedStatement parentOfSubAccount;

	public CapitalAccountTable(Connection connection) throws SQLException {
		super(connection);

		properties.put("Name", new AccountingProperty("\"name\"", Type.STRING));
		properties.put("Comment", new AccountingProperty("\"comment\"", Type.STRING));
		properties.put("Abbreviation", new AccountingProperty("\"abbreviation\"", Type.STRING));

		navigationProperties.put("ParentAccount",
				new ExpandedItem("ParentAccount", this, "\"net_sf_jmoney_capitalAccount_subAccount\""));
	}

	@Override
	public void init(TableReaderFactory tableFactory) throws SQLException {
		accountTable = tableFactory.getAccountTableReader();

		parentOfSubAccount = connection.prepareStatement("select " + getColumnSelectors() + " from NET_SF_JMONEY_CAPITALACCOUNT as CHILD join NET_SF_JMONEY_CAPITALACCOUNT on CHILD.\"net_sf_jmoney_capitalAccount_subAccount\" = NET_SF_JMONEY_CAPITALACCOUNT.\"_ID\" join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_ACCOUNT.\"_ID\" = NET_SF_JMONEY_CAPITALACCOUNT.\"_ID\" where CHILD.\"_ID\" = ?");

		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where NET_SF_JMONEY_ACCOUNT.\"_ID\" = ?");
		subAccountsByAccount = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where \"net_sf_jmoney_capitalAccount_subAccount\" = ?");
	}

	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_CAPITALACCOUNT inner join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_CAPITALACCOUNT.\"_ID\" = NET_SF_JMONEY_ACCOUNT.\"_ID\"";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_CAPITALACCOUNT";
	}
	
	@Override
	public String getColumnSelectors() {
		// The accounting tables may self-join (to get parent), so qualify all columns with the table id.
		return accountTable.getColumnSelectors() + ", NET_SF_JMONEY_CAPITALACCOUNT.\"comment\", NET_SF_JMONEY_CAPITALACCOUNT.\"abbreviation\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "ACCOUNT_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		/* 
		 * We don't return the decimal places here because all amounts are converted from long
		 * to decimal by this driver.  The number of decimal places is really used for conversion
		 * of money amounts to pennies for internal storage of integer values.  Possibly consumers
		 * could use this information for formatting or rounding purposes but that is unlikely.
		 */
		
		int id = result.getInt("ACCOUNT_ID");
		String name = result.getString("ACCOUNT_NAME");
		String comment = result.getString("comment");
		String abbreviation = result.getString("abbreviation");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Name", name))
				.addProperty(createPrimitive("Comment", comment))
				.addProperty(createPrimitive("Abbreviation", abbreviation));
		entity.setId(createId(AccountsEdmProvider.ES_CAPITAL_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_CAPITAL_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("ACCOUNT_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_CAPITAL_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_CAPITAL_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "ParentAccount":
			return readParentAccountExpectingEntity(uriInfo, resourceParts, index, key, expandOption);

		}
		return null; // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}
		
		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			return accountTable.readEntries(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "ParentAccount":
			return readParentAccount(uriInfo, resourceParts, index, key, filterOption, expandOption);
		case "SubAccounts":
			return readSubAccounts(uriInfo, resourceParts, index, key, filterOption, expandOption);
		}
		
		throw new ODataApplicationException("Unexpected navigation from CapitalAccount: " + navProp.getName(), 1, Locale.ENGLISH);
	}

	public EntityCollection readSubAccounts(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			FilterOption filterOption, ExpandOption expandOption)
			throws ODataApplicationException, SQLException {

		CapitalAccountTable capitalAccountTable = this;
		
		EntityCollection entitySet = new EntityCollection();

		if (index + 2 == resourceParts.size()) {
			// This is the last part, so return this result set as entities

			
			// TODO much of this is duplicated in AccountTable.executeQuery.
			// We should call a method on entryTable to do this.
			
			// Because this is the last part, we check now for expanded items.
			List<Expansion> expandedItems = capitalAccountTable.buildExpandedItems(expandOption);
			
			String columnsPartOfQuery = capitalAccountTable.getColumnSelectors();
			String tablesPartOfQuery = "NET_SF_JMONEY_CAPITALACCOUNT left outer join NET_SF_JMONEY_ACCOUNT on NET_SF_JMONEY_CAPITALACCOUNT.\"_ID\" = NET_SF_JMONEY_ACCOUNT.\"_ID\"";

			for (Expansion expansion : expandedItems) {
				ExpandedItem expandedItem = expansion.navigationProperty; 
				columnsPartOfQuery += ", " + expandedItem.getReader().getColumnSelectors();
				tablesPartOfQuery += " left outer join " + expandedItem.getReader().getTableName() + " on NET_SF_JMONEY_CAPITALACCOUNT." + expandedItem.foreignKeyName + " = " + expandedItem.getReader().getTableName() + ".\"_ID\"";
			}
			
			String basicQuery = "select " + columnsPartOfQuery + " from " + tablesPartOfQuery + " where \"net_sf_jmoney_capitalAccount_subAccount\" = ?";
			if (filterOption != null) {
				String whereClause = buildWhereClause(filterOption);
				basicQuery = basicQuery + " where " + whereClause;
			}
			
			PreparedStatement entriesByAccount = connection.prepareStatement(basicQuery);
			
			entriesByAccount.setInt(1, key);
			ResultSet result = entriesByAccount.executeQuery();

			while (result.next()) {
				Entity el = capitalAccountTable.createEntityFromResult(result, expandedItems);
				entitySet.getEntities().add(el);
			}

			return entitySet;
		} else {
			// There are more parts to join
			subAccountsByAccount.setInt(1, key);
			ResultSet result = subAccountsByAccount.executeQuery();

			while (result.next()) {
				int accountId = result.getInt("ACCOUNT_ID");
				EntityCollection entitiesForThisSubAccount = this.readAndJoinCollection(uriInfo, resourceParts, index + 1, accountId, filterOption, expandOption); 
				entitySet.getEntities().addAll(entitiesForThisSubAccount.getEntities());
			}

			return entitySet;
		}
	}

	public Entity readParentAccountExpectingEntity(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			ExpandOption expandOption)
			throws SQLException {

		CapitalAccountTable capitalAccountTable = this;
		
		parentOfSubAccount.setInt(1, key);
		ResultSet parentAccountResult = parentOfSubAccount.executeQuery();

		if (index + 2 == resourceParts.size()) {
			// This is the last part, so return this result set as entities
			parentAccountResult.next();
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity el = capitalAccountTable.createEntityFromResult(parentAccountResult, expandedItems);
			return el;
		} else {
			// There are more parts to join
			parentAccountResult.next();
			int parentAccountId = parentAccountResult.getInt("ACCOUNT_ID");
			Entity entityForThisAccount = capitalAccountTable.readAndJoin(uriInfo, resourceParts, index + 1, parentAccountId, expandOption); 
			return entityForThisAccount;
		}
	}

	public EntityCollection readParentAccount(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			FilterOption filterOption, ExpandOption expandOption)
			throws SQLException, ODataApplicationException {

		CapitalAccountTable capitalAccountTable = this;
		
		parentOfSubAccount.setInt(1, key);
		ResultSet parentAccountResult = parentOfSubAccount.executeQuery();

		if (index + 2 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		} else {
			// There are more parts to join
			parentAccountResult.next();
			int parentAccountId = parentAccountResult.getInt("ACCOUNT_ID");
			EntityCollection entitiesForThisParentAccount = capitalAccountTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, parentAccountId, filterOption, expandOption); 
			return entitiesForThisParentAccount;
		}
	}
}
