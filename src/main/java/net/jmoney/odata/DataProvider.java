/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.ex.ODataException;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;

public class DataProvider {

	private TableReaderFactory tableFactory;
	
	public DataProvider() throws ODataApplicationException {
		String url = "jdbc:derby:C:\\Users\\Nigel\\Documents\\JmoneyAccounts";
		String user = "sa";
		String password = "sa";
		
		try {
			Connection connection = DriverManager.getConnection(url, user, password);
			tableFactory = new TableReaderFactory(connection);
		} catch (SQLException e) {
			throw new ODataApplicationException(e.getMessage(), 500, Locale.ENGLISH);
		}
	}

//	public EntityCollection readAll(EdmEntitySet edmEntitySet, UriInfo uriInfo) throws SQLException, ODataApplicationException {
//		TableReader reader = getReader(edmEntitySet);
//		return reader.readAll(uriInfo);
//	}
	
	public TableReader getReader(EdmEntitySet edmEntitySet) throws SQLException, ODataApplicationException {
		switch (edmEntitySet.getName()) {
		case "Accounts":
			return tableFactory.getAccountTableReader();
		case "Categories":
			return tableFactory.getIncomeExpenseAccountTable();
		case "CapitalAccounts":
			return tableFactory.getCapitalAccountTable();
		case "BankAccounts":
			return tableFactory.getBankAccountTable();
		case "StockAccounts":
			return tableFactory.getStockAccountTable();
		case "Entries":
			return tableFactory.getEntryTableReader();
		case "Transactions":
			return tableFactory.getTransactionTableReader();
		case "Commodities":
			return tableFactory.getCommodityTableReader();
		case "Currencies":
			return tableFactory.getCurrencyTableReader();
		default:
			throw new ODataApplicationException("table not found: " + edmEntitySet.getName(), 500, Locale.ENGLISH);
		}
	}

	public Entity read(final EdmEntitySet edmEntitySet, final List<UriParameter> keys, ExpandOption expandOption) throws DataProviderException, ODataApplicationException {
		if (keys.size() != 1) {
			throw new DataProviderException("table must have one key field: " + edmEntitySet.getName());
		}
		String keyValueText = keys.get(0).getText();
		int keyValue = Integer.valueOf(keyValueText);
		
		try {
			TableReader tableReader = getReader(edmEntitySet);
			List<Expansion> expandedItems = tableReader.buildExpandedItems(expandOption);
			return tableReader.read(keyValue, expandedItems);
		} catch (SQLException e) {
			throw new DataProviderException("SQL exception occured while reading row " + keyValue + " from " + edmEntitySet.getName(), e);
		}
	}

	public static class DataProviderException extends ODataException {
		private static final long serialVersionUID = 5098059649321796156L;

		public DataProviderException(String message, Throwable throwable) {
			super(message, throwable);
		}

		public DataProviderException(String message) {
			super(message);
		}
	}
}
