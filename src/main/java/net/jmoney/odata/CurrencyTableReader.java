/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class CurrencyTableReader extends TableReader {

	private PreparedStatement lookupById;

	public CurrencyTableReader(Connection connection) throws SQLException {
		super(connection);

		properties.put("Name", new AccountingProperty("\"name\"", Type.STRING));
		properties.put("Code", new AccountingProperty("\"code\"", Type.STRING));
	}

	@Override
	public void init(TableReaderFactory tableReaderFactory) throws SQLException {
		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where NET_SF_JMONEY_COMMODITY.\"_ID\" = ?");
	}

	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_CURRENCY inner join NET_SF_JMONEY_COMMODITY on NET_SF_JMONEY_CURRENCY.\"_ID\" = NET_SF_JMONEY_COMMODITY.\"_ID\"";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_COMMODITY";
	}
	
	@Override
	public String getColumnSelectors() {
		return "NET_SF_JMONEY_CURRENCY.\"_ID\" as CURRENCY_ID, NET_SF_JMONEY_COMMODITY.\"name\" as CURRENCY_NAME, \"code\""; 
	}
	
	@Override
	public String getIdAlias() {
		return "CURRENCY_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		/* 
		 * We don't return the decimal places here because all amounts are converted from long
		 * to decimal by this driver.  The number of decimal places is really used for conversion
		 * of money amounts to pennies for internal storage of integer values.  Possibly consumers
		 * could use this information for formatting or rounding purposes but that is unlikely.
		 */
		
		int id = result.getInt("CURRENCY_ID");
		String name = result.getString("CURRENCY_NAME");
		String code = result.getString("code");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Name", name))
				.addProperty(createPrimitive("Code", code));
		entity.setId(createId(AccountsEdmProvider.ES_CURRENCIES_NAME, id));
		entity.setType(AccountsEdmProvider.ET_CURRENCY.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("CURRENCY_ID");
		
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_CURRENCIES_NAME, id));
		entity.setType(AccountsEdmProvider.ET_CURRENCY.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		List<Expansion> expandedItems = buildExpandedItems(expandOption);
		Entity thisOne = read(key, expandedItems);
		
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}
		
//		return readAndJoinCollection();
		return null; // TODO
	}
}
