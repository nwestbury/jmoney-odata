/*
 * Copyright � Nigel Westbury 2019
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

public class ExpandedCollection {

	private String navigationPropertyName;
	
	private TableReader reader;

	public final String foreignKeyName;

	public ExpandedCollection(String navigationPropertyName, TableReader reader, String foreignKeyName) {
		this.navigationPropertyName = navigationPropertyName;
		this.reader = reader;
		this.foreignKeyName = foreignKeyName;
	}
	
	public TableReader getReader() {
		return reader;
	}

	public String getNavigationPropertyName() {
		return navigationPropertyName;
	}

}
