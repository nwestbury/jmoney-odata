/*
 * Copyright � Nigel Westbury 2018
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at https://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law
 * or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under
 * the License.
 */
package net.jmoney.odata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;

import net.jmoney.odata.WherePart.Type;

public class AccountTableReader extends TableReader {

	private TableReader entryTable;
	
	private PreparedStatement lookupById;
	
	private PreparedStatement entriesByAccount;
	
	public AccountTableReader(Connection connection) throws SQLException {
		super(connection);

		properties.put("Name", new AccountingProperty("\"name\"", Type.STRING));
	}
	
	@Override
	public void init(TableReaderFactory tableFactory) throws SQLException {
		entryTable = tableFactory.getEntryTableReader();

		lookupById = connection.prepareStatement("select " + getColumnSelectors() + " from " + getTableJoins() + " where \"_ID\" = ?");
		entriesByAccount = connection.prepareStatement("select " + entryTable.getColumnSelectors() + " from " + entryTable.getTableJoins() + " where \"account\" = ?");
	}

	@Override
	public String getTableJoins() {
		return "NET_SF_JMONEY_ACCOUNT";
	}
	
	@Override
	public String getTableName() {
		return "NET_SF_JMONEY_ACCOUNT";
	}
	
	@Override
	public String getColumnSelectors() {
		return "NET_SF_JMONEY_ACCOUNT.\"_ID\" as ACCOUNT_ID, NET_SF_JMONEY_ACCOUNT.\"name\" as ACCOUNT_NAME"; 
	}
	
	@Override
	public String getIdAlias() {
		return "ACCOUNT_ID";
	}
	
	@Override
	protected Entity createEntityFromResult(ResultSet result, List<Expansion> expandedItems) throws SQLException {
		int id = result.getInt("ACCOUNT_ID");
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id))
				.addProperty(createPrimitive("Name", result.getString("ACCOUNT_NAME")));
		entity.setId(createId(AccountsEdmProvider.ES_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity createEntityFromResultWithIdOnly(ResultSet result) throws SQLException {
		int id = result.getInt("ACCOUNT_ID");
		Entity entity = new Entity()
				.addProperty(createPrimitive("Id", id));
		entity.setId(createId(AccountsEdmProvider.ES_ACCOUNTS_NAME, id));
		entity.setType(AccountsEdmProvider.ET_ACCOUNT.getFullQualifiedNameAsString());
		return entity;
	}

	@Override
	protected Entity read(int id, List<Expansion> expandedItems) throws SQLException {
		lookupById.setInt(1, id);
		ResultSet result = lookupById.executeQuery();
		if (!result.next()) {
			return null;
		}
		Entity entity = createEntityFromResult(result, expandedItems);
		return entity;
	}

	@Override
	public Entity readAndJoin2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, ExpandOption expandOption) throws SQLException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			List<Expansion> expandedItems = buildExpandedItems(expandOption);
			Entity thisOne = read(key, expandedItems);
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, FilterOption filterOption, ExpandOption expandOption) throws ODataApplicationException, SQLException {
		EntityCollection thisOne = readAll(uriInfo, filterOption, expandOption);
		
		int index = 0;
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins, so this is it.
			return thisOne;
		}
		
		return null;  // TODO
	}

	@Override
	public EntityCollection readAndJoinCollection2(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key, FilterOption filterOption, ExpandOption expandOption) throws SQLException, ODataApplicationException {
		if (index + 1 == resourceParts.size()) {
			// We are done with the joins but we expected to end with a collection so we fail.
			throw new ODataApplicationException("Can't get collection from this URL", 1, Locale.ENGLISH);
		}

		UriResourceNavigation navProperty = (UriResourceNavigation)resourceParts.get(index + 1);
		EdmNavigationProperty navProp = navProperty.getProperty();
		
		switch(navProp.getName()) {
		case "Entries":
			return readEntries(uriInfo, resourceParts, index, key, filterOption, expandOption);
		}
		return null; // TODO
	}

	public EntityCollection readEntries(UriInfo uriInfo, List<UriResource> resourceParts, int index, int key,
			FilterOption filterOption, ExpandOption expandOption)
			throws ODataApplicationException, SQLException {
		
		EntityCollection entitySet = new EntityCollection();

		if (index + 2 == resourceParts.size()) {
			// This is the last part, so return this result set as entities

			
			// TODO much of this is duplicated in entryTable.executeQuery.
			// We should call a method on entryTable to do this.
			
			// Because this is the last part, we check now for expanded items.
			List<Expansion> expandedItems = entryTable.buildExpandedItems(expandOption);
			
			String columnsPartOfQuery = entryTable.getColumnSelectors();
			String tablesPartOfQuery = entryTable.getTableName();

			for (Expansion expansion : expandedItems) {
				ExpandedItem expandedItem = expansion.navigationProperty; 
				columnsPartOfQuery += ", " + expandedItem.getReader().getColumnSelectors();
				tablesPartOfQuery += " left outer join " + expandedItem.getReader().getTableName() + " on NET_SF_JMONEY_ENTRY." + expandedItem.foreignKeyName + " = " + expandedItem.getReader().getTableName() + ".\"_ID\"";
			}
			
			String basicQuery = "select " + columnsPartOfQuery + " from " + tablesPartOfQuery + " where \"account\" = ?";
			if (filterOption != null) {
				String whereClause = buildWhereClause(filterOption);
				basicQuery = basicQuery + " where " + whereClause;
			}
			
			PreparedStatement entriesByAccount = connection.prepareStatement(basicQuery);
			
			entriesByAccount.setInt(1, key);
			ResultSet result = entriesByAccount.executeQuery();

			while (result.next()) {
				Entity el = entryTable.createEntityFromResult(result, expandedItems);
				entitySet.getEntities().add(el);
			}

			return entitySet;
		} else {
			// There are more parts to join
			entriesByAccount.setInt(1, key);
			ResultSet result = entriesByAccount.executeQuery();

			while (result.next()) {
				int entryId = result.getInt("ENTRY_ID");
				EntityCollection entitiesForThisEntry = entryTable.readAndJoinCollection(uriInfo, resourceParts, index + 1, entryId, filterOption, expandOption); 
				entitySet.getEntities().addAll(entitiesForThisEntry.getEntities());
			}

			return entitySet;
		}
	}
}
